package com.example.tiptime

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Matchers.containsString

@RunWith(AndroidJUnit4::class)
class CalculatorTests {

    @get:Rule()
    val activity = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun calculate_20_percent_tip() {
        // escribe en el input de la propina
        onView(withId(R.id.const_of_service_edit_text))
            .perform(typeText("50.00"))

        // hacemos click al botonCalcular
        onView(withId(R.id.calculate_button)).perform(click())

        // obtenemos el valor del resultado
        onView(withId(R.id.tip_result)).check(matches(withText(containsString("10"))))
    }
}