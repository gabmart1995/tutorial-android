package com.example.dice

import org.junit.Test

import org.junit.Assert.assertTrue


class ExampleUnitTest {
    @Test
    fun generateNumber() {
        val diceRoll = Dice(6).roll()
        assertTrue(
            "The value of rollResult was not between 1 and 6",
            diceRoll in 1..6
        )
    }
}