package com.example.dice

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.activity.ComponentActivity

class MainActivity : ComponentActivity() {
    private fun rollDice(diceImage: ImageView) {
        val diceRoll = Dice(6).roll()

        // Determine which drawable resource ID to use based on the dice roll
        val drawableResource = when (diceRoll) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        // change the image
        diceImage.setImageResource(drawableResource)
        diceImage.contentDescription = diceRoll.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // selector the button add event listener
        val rollButton: Button = findViewById(R.id.roll)
        rollButton.setOnClickListener {
            rollDice(findViewById(R.id.image))
            rollDice(findViewById(R.id.image2))
        }

        // first throw
        rollDice(findViewById(R.id.image))
        rollDice(findViewById(R.id.image2))
    }
}

class Dice(private val numSides: Int) {
    fun roll(): Int {
        return (1..numSides).random()
    }
}
